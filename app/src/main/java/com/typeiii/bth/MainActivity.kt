package com.typeiii.bth

import android.Manifest
import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.typeiii.bth.helpers.BluetoothSerial
import com.typeiii.bth.databinding.ActivityMainBinding
import android.graphics.drawable.AnimationDrawable
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.karumi.dexter.listener.multi.SnackbarOnAnyDeniedMultiplePermissionsListener
import com.karumi.dexter.listener.multi.CompositeMultiplePermissionsListener
import com.typeiii.bth.helpers.LocationManager


class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    private var bluetoothSerial: BluetoothSerial? = null
    private var allPermissionsListener: MultiplePermissionsListener? = null

    private val deviceName = "HC-06"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        supportActionBar?.hide()

        checkPermissions()

        connectToBluetoothDevice()

        getAccuracy()

        setClickListener()
    }


    private fun checkPermissions() {

        val permissionListener = object : MultiplePermissionsListener {
            override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                for (response in report.grantedPermissionResponses) {
                    if (response.permissionName == Manifest.permission.ACCESS_FINE_LOCATION ) {
                        getAccuracy()
                    }
                }
            }

            override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?,
                                                            token: PermissionToken?) {
            }

        }

        allPermissionsListener = CompositeMultiplePermissionsListener(permissionListener,
                SnackbarOnAnyDeniedMultiplePermissionsListener.Builder.with(binding.view,
                        R.string.all_permissions_denied_feedback)
                        .withOpenSettingsButton(R.string.permission_rationale_settings_button_text)
                        .build())


        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.BLUETOOTH,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(allPermissionsListener)
                .check()
    }

    private fun connectToBluetoothDevice() {
        bluetoothSerial = BluetoothSerial(this, object : BluetoothSerial.MessageHandler {
            override fun connected() {
                binding.button.isEnabled = true
                binding.button.text = getString(R.string.record)
                binding.button.setBackgroundResource(R.drawable.ic_btn_connected)
            }

            override fun disConnected() {
                binding.button.text = getString(R.string.disconnected)
                binding.button.setBackgroundResource(R.drawable.ic_btn_normal)
            }

            override fun read(x: Float, y: Float, z: Float, temp: Float) {

                runOnUiThread {
                    binding.tvXvalue.text = String.format("% 10.2fg", x)
                    binding.tvYvalue.text = String.format("% 10.2fg", y)
                    binding.tvZvalue.text = String.format("% 10.2fg", z)
                    binding.tvTempValue.text = String.format("% 10.2f℃", temp)
                }
            }

            override fun read(bufferSize: Int, buffer: ByteArray): Int {
                return 0
            }
        }, deviceName)
        bluetoothSerial?.connect()
    }

    private fun getAccuracy() {
        LocationManager(object : LocationManager.PositionListener {
            override fun getAccuracy(accuracy: Float) {
                binding.tvAccuValue.text = String.format("%.2fm", accuracy)
            }

        }, true).connect()
    }


    private fun setClickListener() {
        binding.button.setOnClickListener {

            if (bluetoothSerial != null && bluetoothSerial!!.isRecording()!!) {
                binding.button.text = getString(R.string.record)
                binding.button.setBackgroundResource(R.drawable.ic_btn_normal)
            } else {
                binding.button.text = getString(R.string.stop)

                val drawable = AnimationDrawable()
                val image1 = getDrawable(R.drawable.ic_btn_normal)
                val image2 = getDrawable(R.drawable.ic_btn_recoding)
                drawable.addFrame(image1, 1000)
                drawable.addFrame(image2, 1000)
                drawable.isOneShot = false

                binding.button.background = drawable
                drawable.start()
            }


            bluetoothSerial?.setRecord()
        }
    }

}


