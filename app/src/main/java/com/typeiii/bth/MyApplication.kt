package com.typeiii.bth

import android.app.Application
import android.content.Context

class MyApplication: Application() {

    //private var sApplication: Application? = null

    companion object{

        lateinit var sApplication: Application

        private fun getApplication(): Application? {
            return sApplication
        }

        fun getContext(): Context? {
            return getApplication()?.applicationContext
        }
    }

    override fun onCreate() {
        super.onCreate()
        sApplication = this
    }

}