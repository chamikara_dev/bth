package com.typeiii.bth.helpers

import android.annotation.SuppressLint
import android.bluetooth.BluetoothSocket
import android.bluetooth.BluetoothDevice
import android.support.v4.content.LocalBroadcastManager
import android.content.Intent
import android.os.AsyncTask
import android.bluetooth.BluetoothAdapter
import android.content.IntentFilter
import android.content.BroadcastReceiver
import android.content.Context
import android.util.Log
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.util.*


class BluetoothSerial(internal var context: Context, internal var messageHandler: MessageHandler, devicePrefix: String) {

    internal var connected = false

    internal var bluetoothDevice: BluetoothDevice? = null

    internal var serialSocket: BluetoothSocket? = null

    internal var serialInputStream: InputStream? = null

    internal var serialOutputStream: OutputStream? = null

    private var serialReader: SerialReader? = null

    internal var connectionTask: AsyncTask<Void, Void, BluetoothDevice>? = null

    internal var devicePrefix: String


    private val MAX_BYTES = 125

    internal var mConnectedThread: ConnectedThread? = null

    /**
     * Listens for discount message from bluetooth system and restablishing a connection
     */
    private val bluetoothReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            val eventDevice = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)

            if (BluetoothDevice.ACTION_ACL_DISCONNECTED == action) {
                if (bluetoothDevice != null && bluetoothDevice == eventDevice) {
                    Log.i(BMX_BLUETOOTH, "Received bluetooth disconnect notice")

                    //clean up any streams
                    close()

                    //reestablish connect
                    connect()

                    LocalBroadcastManager.getInstance(context).sendBroadcast(Intent(BLUETOOTH_DISCONNECTED))
                }
            }
        }
    }

    init {
        this.devicePrefix = devicePrefix.toUpperCase()
    }

    fun onPause() {
        context.unregisterReceiver(bluetoothReceiver)
    }

    fun onResume() {
        //listen for bluetooth disconnect
        val disconnectIntent = IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECTED)
        context.registerReceiver(bluetoothReceiver, disconnectIntent)

        //reestablishes a connection is one doesn't exist
        if (!connected) {
            connect()
        } else {
            val intent = Intent(BLUETOOTH_CONNECTED)
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
        }
    }


    /**
     * Initializes the bluetooth serial connections, uses the LocalBroadcastManager when
     * connection is established
     *
     * @param localBroadcastManager
     */
    fun connect() {

        if (connected) {
            Log.e(BMX_BLUETOOTH, "Connection request while already connected")
            return
        }

        if (connectionTask != null && connectionTask!!.status == AsyncTask.Status.RUNNING) {
            Log.e(BMX_BLUETOOTH, "Connection request while attempting connection")
            return
        }

        val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled) {
            return
        }

        val pairedDevices = ArrayList(bluetoothAdapter.bondedDevices)
        if (pairedDevices.size > 0) {
            bluetoothAdapter.cancelDiscovery()

            /**
             * AsyncTask to handle the establishing of a bluetooth connection
             */
            connectionTask = @SuppressLint("StaticFieldLeak")
            object : AsyncTask<Void, Void, BluetoothDevice>() {

                internal var MAX_ATTEMPTS = 30

                internal var attemptCounter = 0

                override fun doInBackground(vararg params: Void): BluetoothDevice? {
                    while (!isCancelled) { //need to kill without calling onCancel

                        for (device in pairedDevices) {
                            if (device.name.toUpperCase().startsWith(devicePrefix)) {
                                Log.i(BMX_BLUETOOTH, attemptCounter.toString() + ": Attempting connection to " + device.name)

                                try {

                                    try {
                                        // Standard SerialPortService ID
                                        val uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB")
                                        serialSocket = device.createRfcommSocketToServiceRecord(uuid)
                                    } catch (ce: Exception) {
                                        serialSocket = connectViaReflection(device)
                                    }

                                    //setup the connect streams
                                    serialSocket!!.connect()
                                    serialInputStream = serialSocket!!.inputStream
                                    serialOutputStream = serialSocket!!.outputStream

                                    connected = true
                                    Log.i(BMX_BLUETOOTH, "Connected to " + device.name)

                                    return device
                                } catch (e: Exception) {
                                    serialSocket = null
                                    serialInputStream = null
                                    serialOutputStream = null
                                    Log.i(BMX_BLUETOOTH, e.message)
                                }

                            }
                        }

                        try {
                            attemptCounter++
                            if (attemptCounter > MAX_ATTEMPTS)
                                this.cancel(false)
                            else
                                Thread.sleep(1000)
                        } catch (e: InterruptedException) {
                            break
                        }

                    }

                    Log.i(BMX_BLUETOOTH, "Stopping connection attempts")

                    val intent = Intent(BLUETOOTH_FAILED)
                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent)

                    return null
                }

                override fun onPostExecute(result: BluetoothDevice) {
                    super.onPostExecute(result)

                    bluetoothDevice = result

                    //start thread responsible for reading from inputstream
                    serialReader = SerialReader()
                    serialReader!!.start()

                    //send connection message
                    val intent = Intent(BLUETOOTH_CONNECTED)
                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent)

                    if (serialSocket != null) {
                        mConnectedThread = ConnectedThread(serialSocket!!, messageHandler)
                        messageHandler.connected()
                        mConnectedThread!!.start()
                    }

                }

            }
            connectionTask!!.execute()
        }
    }

    // see: http://stackoverflow.com/questions/3397071/service-discovery-failed-exception-using-bluetooth-on-android
    @Throws(Exception::class)
    private fun connectViaReflection(device: BluetoothDevice): BluetoothSocket {
        val m = device.javaClass.getMethod("createRfcommSocket", *arrayOf<Class<*>>(Int::class.javaPrimitiveType!!))
        return m.invoke(device, 1) as BluetoothSocket
    }

    @Throws(IOException::class)
    fun available(): Int {
        if (connected)
            return serialInputStream!!.available()

        throw RuntimeException("Connection lost, reconnecting now.")
    }

    @Throws(IOException::class)
    fun read(): Int {
        if (connected)
            return serialInputStream!!.read()

        throw RuntimeException("Connection lost, reconnecting now.")
    }

    @Throws(IOException::class)
    fun read(buffer: ByteArray): Int {
        if (connected)
            return serialInputStream!!.read(buffer)

        throw RuntimeException("Connection lost, reconnecting now.")
    }

    @Throws(IOException::class)
    fun read(buffer: ByteArray, byteOffset: Int, byteCount: Int): Int {
        if (connected)
            return serialInputStream!!.read(buffer, byteOffset, byteCount)

        throw RuntimeException("Connection lost, reconnecting now.")
    }

    @Throws(IOException::class)
    fun write(buffer: ByteArray) {
        if (connected)
            serialOutputStream!!.write(buffer)

        throw RuntimeException("Connection lost, reconnecting now.")
    }

    @Throws(IOException::class)
    fun write(oneByte: Int) {
        if (connected)
            serialOutputStream!!.write(oneByte)

        throw RuntimeException("Connection lost, reconnecting now.")
    }

    @Throws(IOException::class)
    fun write(buffer: ByteArray, offset: Int, count: Int) {
        serialOutputStream!!.write(buffer, offset, count)

        throw RuntimeException("Connection lost, reconnecting now.")
    }

    private inner class SerialReader : Thread() {

        internal var buffer = ByteArray(MAX_BYTES)

        internal var bufferSize = 0

        override fun run() {
            Log.i("serialReader", "Starting serial loop")
            while (!isInterrupted) {
                try {

                    /*
					 * check for some bytes, or still bytes still left in
					 * buffer
					 */
                    if (available() > 0) {

                        val newBytes = read(buffer, bufferSize, MAX_BYTES - bufferSize)
                        if (newBytes > 0)
                            bufferSize += newBytes

                        //Log.d(BMX_BLUETOOTH, "read $newBytes")
                    }

                    if (bufferSize > 0) {
                        val read = messageHandler.read(bufferSize, buffer)

                        // shift unread data to start of buffer
                        if (read > 0) {
                            var index = 0
                            for (i in read until bufferSize) {
                                buffer[index++] = buffer[i]
                            }
                            bufferSize = index
                        }
                    } else {

                        try {
                            Thread.sleep(10)
                        } catch (ie: InterruptedException) {
                            break
                        }

                    }
                } catch (e: Exception) {
                    Log.e(BMX_BLUETOOTH, "Error reading serial data", e)
                }

            }
            Log.i(BMX_BLUETOOTH, "Shutting serial loop")
        }

    }

    /**
     * Reads from the serial buffer, processing any available messages.  Must return the number of bytes
     * consumer from the buffer
     *
     * @author jpetrocik
     */
    interface MessageHandler {
        fun connected()
        fun disConnected()
        fun read(bufferSize: Int, buffer: ByteArray): Int
        fun read(x: Float, y:Float, z:Float, temp:Float )
    }

    fun close() {

        connected = false
        messageHandler.disConnected()

        if (serialReader != null) {
            serialReader!!.interrupt()

            try {
                serialReader!!.join(1000)
            } catch (ie: InterruptedException) {
            }

        }

        try {
            serialInputStream!!.close()
        } catch (e: Exception) {
            Log.e(BMX_BLUETOOTH, "Failed releasing inputstream connection")
        }

        try {
            serialOutputStream!!.close()
        } catch (e: Exception) {
            Log.e(BMX_BLUETOOTH, "Failed releasing outputstream connection")
        }

        try {
            serialSocket!!.close()
        } catch (e: Exception) {
            Log.e(BMX_BLUETOOTH, "Failed closing socket")
        }

        Log.i(BMX_BLUETOOTH, "Released bluetooth connections")

    }

    fun setRecord() {
        mConnectedThread?.setRecord()
    }

    fun isRecording(): Boolean? {
        return  mConnectedThread?.getRecord()
    }

    companion object {
        private val BMX_BLUETOOTH = "BMXBluetooth"

        var BLUETOOTH_CONNECTED = "bluetooth-connection-started"

        var BLUETOOTH_DISCONNECTED = "bluetooth-connection-lost"

        var BLUETOOTH_FAILED = "bluetooth-connection-failed"
    }

}