package com.typeiii.bth.helpers

import android.bluetooth.BluetoothSocket
import android.os.Bundle
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.util.*

internal class ConnectedThread(private val mmSocket: BluetoothSocket,
                               var messageHandler: BluetoothSerial.MessageHandler) : Thread() {
    private val mmInStream: InputStream?
    private val mmOutStream: OutputStream?

    private val fData = FloatArray(31)
    private var strDate: String? = null
    private var strTime: String? = null


    private val queueBuffer = LinkedList<Byte>()
    private val packBuffer = ByteArray(11)

    private var isRecord = false
    private val RecordTimeDifference = 1 // seconds

    init {
        var tmpIn: InputStream? = null
        var tmpOut: OutputStream? = null

        // Get the BluetoothSocket input and output streams
        try {
            tmpIn = mmSocket.inputStream
            tmpOut = mmSocket.outputStream
        } catch (e: IOException) {
        }

        mmInStream = tmpIn
        mmOutStream = tmpOut
    }

    override fun run() {
        val tempInputBuffer = ByteArray(1024)
        var acceptedLen: Int
        var sHead: Byte
        // Keep listening to the InputStream while connected
        var lLastTime = System.currentTimeMillis() // 获取开始时间
        var lLastRecoderTime = System.currentTimeMillis()
        while (true) {

            try {

                acceptedLen = mmInStream!!.read(tempInputBuffer)
                //Log.d("BTL1", "" + acceptedLen)
                for (i in 0 until acceptedLen) queueBuffer.add(tempInputBuffer[i])


                while (queueBuffer.size >= 11) {
                    if (queueBuffer.poll() != 0x55.toByte()) continue
                    sHead = queueBuffer.poll()
                    for (j in 0..8) packBuffer[j] = queueBuffer.poll()

                    when (sHead) {
                    //
                        0x50.toByte() -> {
                            val ms = packBuffer[7].toInt() shl 8 or (packBuffer[6].toInt() and 0xff)
                            strDate = String.format("20%02d-%02d-%02d", packBuffer[0], packBuffer[1], packBuffer[2])
                            strTime = String.format(" %02d:%02d:%02d.%03d", packBuffer[3], packBuffer[4], packBuffer[5], ms)
                        }
                        0x51.toByte() -> {
                            fData[0] = (packBuffer[1].toInt() shl 8 or (packBuffer[0].toInt() and 0xff)) / 32768.0f * 16
                            fData[1] = (packBuffer[3].toInt() shl 8 or (packBuffer[2].toInt() and 0xff)) / 32768.0f * 16
                            fData[2] = (packBuffer[5].toInt() shl 8 or (packBuffer[4].toInt() and 0xff)) / 32768.0f * 16
                            fData[17] = (packBuffer[7].toInt() shl 8 or (packBuffer[6].toInt() and 0xff)) / 100.0f
                        }
                        0x52.toByte() -> {
                            fData[3] = (packBuffer[1].toInt() shl 8 or (packBuffer[0].toInt() and 0xff)) / 32768.0f * 2000
                            fData[4] = (packBuffer[3].toInt() shl 8 or (packBuffer[2].toInt() and 0xff)) / 32768.0f * 2000
                            fData[5] = (packBuffer[5].toInt() shl 8 or (packBuffer[4].toInt() and 0xff)) / 32768.0f * 2000
                            fData[17] = (packBuffer[7].toInt() shl 8 or (packBuffer[6].toInt() and 0xff)) / 100.0f
                        }
                        0x53.toByte() -> {
                            fData[6] = (packBuffer[1].toInt() shl 8 or (packBuffer[0].toInt() and 0xff)) / 32768.0f * 180
                            fData[7] = (packBuffer[3].toInt() shl 8 or (packBuffer[2].toInt() and 0xff)) / 32768.0f * 180
                            fData[8] = (packBuffer[5].toInt() shl 8 or (packBuffer[4].toInt() and 0xff)) / 32768.0f * 180
                            fData[17] = (packBuffer[7].toInt() shl 8 or (packBuffer[6].toInt() and 0xff)) / 100.0f
                        }
                        0x54.toByte() //magnetic field
                        -> {
                            fData[9] = (packBuffer[1].toInt() shl 8 or (packBuffer[0].toInt() and 0xff)).toFloat()
                            fData[10] = (packBuffer[3].toInt() shl 8 or (packBuffer[2].toInt() and 0xff)).toFloat()
                            fData[11] = (packBuffer[5].toInt() shl 8 or (packBuffer[4].toInt() and 0xff)).toFloat()
                            fData[17] = (packBuffer[7].toInt() shl 8 or (packBuffer[6].toInt() and 0xff)) / 100.0f
                        }
                        0x55.toByte() //port
                        -> {
                            fData[12] = (packBuffer[1].toInt() shl 8 or (packBuffer[0].toInt() and 0xff)).toFloat()
                            fData[13] = (packBuffer[3].toInt() shl 8 or (packBuffer[2].toInt() and 0xff)).toFloat()
                            fData[14] = (packBuffer[5].toInt() shl 8 or (packBuffer[4].toInt() and 0xff)).toFloat()
                            fData[15] = (packBuffer[7].toInt() shl 8 or (packBuffer[6].toInt() and 0xff)).toFloat()
                        }
                        0x56.toByte() //Pressure, height
                        -> {
                            fData[16] = (packBuffer[3].toLong() shl 24 or (packBuffer[2].toLong() shl 16) or (packBuffer[1].toLong() shl 8) or packBuffer[0].toLong()).toFloat()
                            fData[17] = (packBuffer[7].toLong() shl 24 or (packBuffer[6].toLong() shl 16) or (packBuffer[5].toLong() shl 8) or packBuffer[4].toLong()).toFloat()
                            fData[17] /= 100f
                        }
                        0x57.toByte() //Latitude and longitude
                        -> {
                            val longitude = packBuffer[3].toLong() shl 24 or (packBuffer[2].toLong() shl 16) or (packBuffer[1].toLong() shl 8) or packBuffer[0].toLong()
                            fData[18] = (longitude.toFloat() / 10000000 + (longitude % 10000000).toFloat().toDouble() / 100000.0 / 60.0).toFloat()
                            val latitude = packBuffer[7].toLong() shl 24 or (packBuffer[6].toLong() shl 16) or (packBuffer[5].toLong() shl 8) or packBuffer[4].toLong()
                            fData[19] = (latitude.toFloat() / 10000000 + (latitude % 10000000).toFloat().toDouble() / 100000.0 / 60.0).toFloat()
                        }
                        0x58.toByte() //Altitude, heading, ground speed
                        -> {
                            fData[20] = ((packBuffer[3].toLong() shl 24 or (packBuffer[2].toLong() shl 16) or (packBuffer[1].toLong() shl 8) or packBuffer[0].toLong()) / 10).toFloat()
                            fData[21] = ((packBuffer[5].toInt() shl 8 or (packBuffer[4].toInt() and 0xff)) / 10).toFloat()
                            fData[22] = ((packBuffer[7].toInt() shl 8 or (packBuffer[6].toInt() and 0xff)) / 1000).toFloat()
                        }
                        0x59.toByte() //Quaternion
                        -> {
                            fData[23] = (packBuffer[1].toInt() shl 8 or (packBuffer[0].toInt() and 0xff)) / 32768.0f
                            fData[24] = (packBuffer[3].toInt() shl 8 or (packBuffer[2].toInt() and 0xff)) / 32768.0f
                            fData[25] = (packBuffer[5].toInt() shl 8 or (packBuffer[4].toInt() and 0xff)) / 32768.0f
                            fData[26] = (packBuffer[7].toInt() shl 8 or (packBuffer[6].toInt() and 0xff)) / 32768.0f
                        }
                        0x5a.toByte() //Number of satellites
                        -> {
                            fData[27] = (packBuffer[1].toInt() shl 8 or (packBuffer[0].toInt() and 0xff)) / 32768.0f
                            fData[28] = (packBuffer[3].toInt() shl 8 or (packBuffer[2].toInt() and 0xff)) / 32768.0f
                            fData[29] = (packBuffer[5].toInt() shl 8 or (packBuffer[4].toInt() and 0xff)) / 32768.0f
                            fData[30] = (packBuffer[7].toInt() shl 8 or (packBuffer[6].toInt() and 0xff)) / 32768.0f
                        }
                    }

                }

                val lTimeNow = System.currentTimeMillis() // Get start time
                if (lTimeNow - lLastTime > 80) {
                    lLastTime = lTimeNow
                    val bundle = Bundle()
                    bundle.putFloatArray("Data", fData)
                    bundle.putString("Date", strDate)
                    bundle.putString("Time", strTime)

                    messageHandler.read(fData[0], fData[1], fData[2], fData[17])
                    if (isRecord && (lTimeNow - lLastRecoderTime > (RecordTimeDifference * 1000))) {
                        Recorder.getInstance().saveToTextFile(fData[0], fData[1], fData[2], fData[17])
                        lLastRecoderTime = lTimeNow
                    }
                }

            } catch (e: IOException) {
                //connectionLost()
                break
            }

        }
    }

    fun write(buffer: ByteArray) {
        try {
            mmOutStream!!.write(buffer)
        } catch (e: IOException) {
        }

    }

    fun cancel() {
        try {
            mmSocket.close()
        } catch (e: IOException) {
        }

    }

    fun setRecord() {
        isRecord = !isRecord
    }

    fun getRecord(): Boolean {
        return isRecord
    }
}