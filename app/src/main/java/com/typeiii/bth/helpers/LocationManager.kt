package com.typeiii.bth.helpers

import android.content.Context
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.os.Looper
import android.util.Log
import com.typeiii.bth.MyApplication

class LocationManager(var listener: PositionListener, var needAccuracy: Boolean) {

    interface PositionListener {
        fun getLatLong(latitude:Double, longitude:Double) { }
        fun getAccuracy(accuracy:Float) { }
    }

    fun connect() {
        val locationListener = object: LocationListener {
            override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {
            }

            override fun onProviderEnabled(p0: String?) {
            }

            override fun onProviderDisabled(p0: String?) {
            }

            override fun onLocationChanged(location: Location?) {

                if (location != null){
                    listener.getLatLong(location.latitude, location.longitude)
                }
            }

        }



        val lm = MyApplication.getContext()?.getSystemService(Context.LOCATION_SERVICE)
                as LocationManager
        try {
            val location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER)
            lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 1f,
                    locationListener, Looper.getMainLooper())

            if (location != null) {
                listener.getLatLong(location.latitude, location.longitude)
                if (needAccuracy) listener.getAccuracy(location.accuracy)
            }



        } catch (e: SecurityException) {
            Log.e("ERROR", "location permission error")
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }




}