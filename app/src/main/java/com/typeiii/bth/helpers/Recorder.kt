package com.typeiii.bth.helpers

import android.os.Environment
import java.text.SimpleDateFormat
import java.util.*
import java.io.*


class Recorder {

    companion object {
        private var instance : Recorder? = null

        fun  getInstance(): Recorder {
            if (instance == null)
                instance = Recorder()

            return instance!!
        }
    }

    private val fileName: String
        get() = "BTH_$currentDate.txt"

    private val currentDate: String
        get() {
            return SimpleDateFormat("ddMyyyy", Locale.US).format(Date())
        }

    private val currentTime: String
        get() {
            return SimpleDateFormat("H:mm:ss", Locale.US).format(Date())
        }

    private var locationLatitude: Double = 0.0
    private var locationLongitude: Double = 0.0

    private fun convertValesToString( x: Float, y: Float, z: Float, temp: Float) : String {
        val xStr = String.format("% 10.2fg", x)
        val yStr = String.format("% 10.2fg", y)
        val zStr = String.format("% 10.2fg", z)
        val tempStr = String.format("% 10.2f℃", temp)

        return "$currentTime | X: $xStr | Y: $yStr | Z: $zStr | Temp: $tempStr " +
                "| Latitude: $locationLatitude | Longitude: $locationLongitude"
    }

    fun saveToTextFile( x: Float, y: Float, z: Float, temp: Float) {

        getLocation()

        try {

            val path = Environment.getExternalStorageDirectory().absolutePath + "/BTH"
            val folder = File(path)
            if (!folder.exists()) folder.mkdirs()

            val file = File(folder, fileName)
            if (!file.exists()) file.createNewFile()
            //open file for writing
            val out = OutputStreamWriter(FileOutputStream(file, true))

            out.write(convertValesToString(x, y, z, temp))
            out.write("\n")

            //close file
            out.close()

        } catch (e: java.io.IOException) {
            e.printStackTrace()
        }

    }

    private fun getLocation() {
        LocationManager(object : LocationManager.PositionListener{
            override fun getLatLong(latitude: Double, longitude: Double) {
                locationLatitude = latitude
                locationLongitude = longitude
            }

        }, false).connect()
    }

}